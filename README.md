vue-playground
--------------

A little playground for me to practice using [vue.js](http://vuejs.org/) before I begin using it in larger projects.

Instructions
------------

The *editor.html* has all of the external files included through various CDNs. Thus, no special build tools are required right now.

That may change.

In theory, this should work fine via local filesystem access. However, it might work better through typical server access. This *will* be come mandatory when asynchronous calls are made.

For example:
```
cd ~/vue-playground/
php -S localhost:8001
```
Then access via `http://localhost:8001/editor.html`.
