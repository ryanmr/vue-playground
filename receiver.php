<?php

if (!empty($_POST)) {
  $data = $_POST;

  echo json_encode($data);

} else {
  echo ['error' => 'no data supplied'];
}
