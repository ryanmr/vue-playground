
var episodes = [{"label":"At The Nexus #435: The Next Epiosode","value":"745"},{"label":"The Fringe #295: The Fringe","value":"742"},{"label":"The Fringe #322: PK 2 \u2014 The Paul","value":"738"},{"label":"Eight Bit #119: Getting Jokes On The First Pass","value":"732"},{"label":"The Fringe #317: TED 4 \u2014 Bush in the Back","value":"728"},{"label":"Eight Bit #118: We Saw Their Details","value":"725"},{"label":"The Fringe #314: EB 117 \u2014 The Decker Needs Food Show","value":"722"},{"label":"Eight Bit #117: The Dam(n) Level is Way Bigger","value":"721"},{"label":"Control Structure #85: Smokey The Pi","value":"717"},{"label":"The Fringe #311: EB 115 \u2014 Hold These Keys Please","value":"716"}];
var people = [{"label":"Ryan Rampersad","value":"1"},{"label":"Sam Ebertz","value":"3"},{"label":"Ian Decker","value":"5"},{"label":"Andrew Bailey","value":"6"},{"label":"Christopher Thompson","value":"7"},{"label":"Brian Mitchell","value":"8"},{"label":"Leif Park Jordan","value":"10"},{"label":"Declan McCrory","value":"12"},{"label":"Sam Roth","value":"14"},{"label":"Tai Rielle","value":"17"}];
var media = [{"id":"2203","type":"mp3","mime":"","meta":"","length":"1098","size":"10905564","url":"http:\/\/s3.amazonaws.com\/the-nexus-tv\/podcasts\/ted\/ted4.mp3","hidden":"0","episode_id":"727","human_length":"0:18:18","human_size":"10.40 MB"}];


var thing = new Vue({

    el: '#episode',

    data: {},

    computed: {
      parents: function() {
        return this.episode.related.filter(function(episode){
          return episode.type === 'parent';
        });
      },
      fringes: function() {
        return this.episode.related.filter(function(episode){
          return episode.type === 'fringe';
        });
      }
    },

    ready: function() {
      console.log('ready');
      var self = this;

      // this is the initial data sent down from the server
      var initial = {
          episode: {
            name: '',
            number: null,
            series: 5,
            state: null,
            hidden: false,
            nsfw: false,
            created_at: "2015-06-01 02:00:03",
            updated_at: "2015-06-01 02:00:03",

            content: '',
            description: '',

            // related: {
            //   parent: '',
            //   parent_id: '',
            //   fringe: '',
            //   fringe_id: ''
            // },

            related: [],

            people: [],

            media: []

          },
          series: [
            {text: 'At The Nexus', value: 2},
            {text: 'The Fringe', value: 5},
            {text: 'PodKit', value: 9}
          ],
          states: [
            {text: 'Draft', value: 'draft'},
            {text: 'Preview', value: 'preview'},
            {text: 'Scheduled', value: 'scheduled'},
            {text: 'Published', value: 'published'},
          ],
          peopleTypes: [
            {text: 'Guest', value: 'guest'},
            {text: 'Host', value: 'host'}
          ],

          related: [],
          people: [],

          meta: {
            valid: false
          },

          queries: {
            people: '',
            related: {
              parent: '',
              fringe: ''
            }
          }
      };

      // simulate loading this via ajax
      setTimeout(function(){
        self.$data = initial;
      }.bind(this), 10);

      setTimeout(function(){

        this.related = episodes;
        this.people = people;

        $(this.$els.relatedparent).autocomplete({},[
          {
            source: function(query, cb){
              cb(this.related);
            }.bind(this),
            displayKey: 'label'
          }
        ]).on('autocomplete:selected', function(e, s, d){
          var obj = {id: s.value, name: s.label, type: 'parent'};
          this.episode.related.push(obj);
          $(e.target).autocomplete('val', '');
          this.queries.related.parent = '';
        }.bind(this));

        $(this.$els.relatedfringe).autocomplete({},[
          {
            source: function(query, cb){
              cb(this.related);
            }.bind(this),
            displayKey: 'label'
          }
        ]).on('autocomplete:selected', function(e, s, d){
          var obj = {id: s.value, name: s.label, type: 'fringe'};
          this.episode.related.push(obj);
          $(e.target).autocomplete('val', '');
          this.queries.related.fringe = '';
        }.bind(this));

        $(this.$els.peoplequery).autocomplete({}, [
          {
            source: function(query, cb) {
              cb(this.people);
            }.bind(this),
            displayKey: 'label'
          }
        ]).on('autocomplete:selected', function(e,s,d){
          var obj = {id: s.value, name: s.label, type: 'guest'};
          this.episode.people.push(obj);
          this.queries.people = '';
        }.bind(this))
          .on('autocomplete:closed', function(e){
          this.queries.people = '';
          $(e.target).autocomplete('val', '');
        }.bind(this));


      }.bind(this), 15);

    },

    methods: {
      submitEpisode: function(e){
        e.preventDefault();

        $.ajax({
          url: 'receiver.php', method: 'post',
          data: this.episode
        }).done(function(data){
          console.log(data);
        });

      },
      removePerson: function(person) {
        this.episode.people.$remove(person);
      },
      removeRelated: function(episode) {
        this.episode.related.$remove(episode);
      },
      newMP3Media: function(e) {
        e.preventDefault();
        var media = {
          type: 'mp3',
          url: '',
        };
        this.episode.media.push(media);
      },
      checkMedia: function(media, e) {
        // if media has an id, its already been checked
        // otherwise, it's fresh

        // find the index
        var index = this.episode.media.indexOf(media);
        if (index >= 0) {
          this.episode.media.$set(index, {"id":"2203","type":"mp3","mime":"","meta":"","length":"1098","size":"10905564","url":"http:\/\/s3.amazonaws.com\/the-nexus-tv\/podcasts\/ted\/ted4.mp3","hidden":"0","episode_id":"727","human_length":"0:18:18","human_size":"10.40 MB"});
        }

      },
      removeMedia: function(media, e) {
        this.episode.media.$remove(media);
      },
      isValidEpisode: function() {
        if (this.episode.series && this.episode.number) {
          // pretend to ask ajax if this is right
          console.log('this is right');
          this.meta.valid = true;
        }
      }
    }

});
